# 1. 说明

这是百问网系列课程之《裸机核心与RTOS必备》配套的文档、源码。
视频观看地址：http://www.100ask.net
配套开发板：100ASK_STM32F103、100ASK_STM32MP157、100ASK_IMX6ULL。

* 同时讲解STM32F103、STM32MP157、IMX6ULL三款开发板
* 同时讲解cortex M3/M4/A7三种处理器
* 同时使用keil/GCC两套开发工具
* 让你能从单片机开发，顺利切入Linux开发



# 2. 更新说明

* 2020.10.13 初始版本

* 2020.10.15 增加"ARM汇编模拟器"

* 2020.10.23 发布《05-08 课》、《第9课第1节》对于的资料、

* 2020.11.07 完结《第9课》

* 2020.11.16 完结《第10课》

* 2021.05.22 解决STM32MP157 UART BUG，升级STM32烧写工具为V2.5

  ```shell
  stm32mp157 uart.c: 初始化PLL3前先禁止它
  doc_and_source_for_mcu_mpu\STM32MP157\开发板配套资料\软件\烧写软件\en.stm32cubeprog.zip
  ```

  

