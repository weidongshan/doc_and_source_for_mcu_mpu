# 代码段重定位

## 1. 代码段不重定位的后果
谁执行了数据段的重定位？
谁清除了BSS段？
都是程序自己做的，也就是代码段那些指令实现的。
代码段并没有位于它的链接地址上，并没有重定位，为什么它也可以执行？

因为重定位之前的代码是使用**位置无关码**写的，后面再说。

如果代码段没有重定位，则不能使用链接地址来调用函数：

* 汇编中

  ```
  ldr  pc, =main   ; 这样调用函数时，用到main函数的链接地址，如果代码段没有重定位，则跳转失败
  ```


* C语言中

  ```c
  void (*funcptr)(const char *s, unsigned int val);
  funcptr = put_s_hex;
  funcptr("hello, test function ptr", 123);
  ```



## 2. 代码段重定位

### 2.1 代码段在哪？多大？

这要看链接脚本，对于MPU的程序，代码段、数据段一般是紧挨着排列的。
所以重定位时，干脆把代码段、数据段一起重定位。

* 链接脚本

```
SECTIONS {
    . = 0xC0200000;   /* 对于STM32MP157设置链接地址为0xC0200000, 对于IMX6ULL设为0x80200000 */

    . = ALIGN(4);
    .text      :
    {
      *(.text)
    }

    . = ALIGN(4);
    __rodata_start = .;
    .rodata : { *(.rodata) }

    . = ALIGN(4);
    .data : { *(.data) }

    . = ALIGN(4);
    __bss_start = .;
    .bss : { *(.bss) *(.COMMON) }
    __bss_end = .;
}
```

对于这样的代码：

```
.text
.global  _start
_start: 				
```

* 确定目的
  
  ```
  ldr r0, =_start
  ```

* 确定源

  ```
  adr  r1, _start
  ```
* 确定长度
  ```
  ldr r3, =__bss_start
  sub r2, r3, r0
  ```
  
  

### 4.2 怎么重定位

```
ldr r0, =_start
adr  r1, _start
ldr r3, =__bss_start
sub r2, r3, r0
bl memcpy
```

## 

## 5. 为什么重定位之前的代码也可以正常运行？

因为重定位之前的代码是使用**位置无关码**写的：

* 只使用相对跳转指令：b、bl

* 不只用绝对跳转指令：

  ```
  ldr pc, =main
  ```

* 不访问全局变量、静态变量、字符串、数组

* 重定位完后，使用绝对跳转指令跳转到XXX函数的链接地址去

  ```
  bl main         // bl相对跳转，程序仍在原来的区域运行
  
  ldr pc, =main   // 绝对跳转，跳到链接地址去运行
  
  ldr r0, =main   // 更规范的写法，支持指令集切换
  blx r0
  ```

  