# 重定位的纯C函数实现

## 1. 怎么得到链接脚本里的值
对于这样的链接脚本，怎么得到其中的`__bss_start`和`    __bss_end`:

```
SECTIONS {
    . = 0xC0200000;   /* 对于STM32MP157设置链接地址为0xC0200000, 对于IMX6ULL设为0x80200000 */

    . = ALIGN(4);
    .text      :
    {
      *(.text)
    }

    . = ALIGN(4);
    __rodata_start = .;
    .rodata : { *(.rodata) }

    . = ALIGN(4);
    .data : { *(.data) }

    . = ALIGN(4);
    __bss_start = .;
    .bss : { *(.bss) *(.COMMON) }
    __bss_end = .;
}
```

### 1.1 汇编代码
```
ldr  r0, =__bss_start
ldr  r1, =__bss_end
```

### 1.2 C语言

* 方法1
声明为外部变量，使用时**需要**使用取址符：

```c
extern unsigned int __bss_start;
extern unsigned int __bss_end;
unsigned int len;
len = (unsigned int)&__bss_end - (unsigned int)&__bss_start;
memset(&__bss_start, 0, len);
```

* 方法2
  声明为外部数组，使用时**不需要**使用取址符：

  ```c
  extern char __bss_start[];
  extern char __bss_end[];
  unsigned int len;
  len = __bss_end - __bss_start;
  memset(__bss_start, 0, len);
  ```

  

## 2. 怎么理解上述代码

 对于这样的C变量：

```c
int g_a;
```

编译的时候会有一个符号表(symbol table)，如下：

| Name | Address  |
| ---- | -------- |
| g_a  | xxxxxxxx |

对于链接脚本中的各类Symbol，有2中声明方式：

```c
extern unsigned int __bss_start;     // 声明为一般变量
extern char __bss_start[];           // 声明为数组
```

不管是哪种方式，它们都会保存在符号表里，比如：

| Name        | Address  |
| ----------- | -------- |
| g_a         | xxxxxxxx |
| __bss_start | yyyyyyyy |

* 对于`int g_a`变量
  * 使用`&g_a`得到符号表里的地址。
* 对于`extern unsigned int __bss_start`变量
  * 要得到符号表中的地址，也是使用`&__bss_start`。
* 对于`extern char __bss_start[]`变量
  * 要得到符号表中的地址，直接使用`__bss_start[]`，不需要加`&`
  * 为什么？`__bss_start本身就表示地址啊


