# 异常与中断的概念引入与处理流程
## 1.1 使用生活实例引入中断

![](lesson\exception_irq\001_mother_son.png)

假设有个大房间里面有小房间，婴儿正在睡觉，他的妈妈在外面看书。
问：这个母亲怎么才能知道这个小孩醒？

1. 过一会打开一次房门，看婴儿是否睡醒，然后接着看书
2. 一直等到婴儿发出声音以后再过去查看，期间都在读书

第一种方法叫做**查询方式**：

* 优点：简单 
* 缺点： 累 

如何写程序？
```c
while(1)
{
	1 read book(读书)
	2 open door(开门)
	  if(小孩还在睡)
		 return(继续读书)
	   else
		 照顾小孩	
}
```

第二种方法叫**中断方式**：

* 优点：不累
* 缺点：复杂

如何写程序：
```c
while(1)
{
	read book
}
中断服务程序() //核心问题：如何被调用？
{
	处理照顾小孩
}
```
## 1.2 母亲如何处理中断

我们还是看看母亲被小孩哭声打断如何照顾小孩？

母亲的处理过程

* 平时看书
* 发生了各种声音，如何处理这些声音
  * 有远处的猫叫（听而不闻，忽略）
  * 门铃声有快递（开门收快递）
  * 小孩哭声（打开房门，照顾小孩）
* 母亲的处理
     * 只会处理门铃声和小孩哭声
       * 先在书中放入书签，合上书(保存现场)
       * 去处理 (调用对应的中断服务程序)
       * 继续看书(恢复现场)

不同情况，不同处理

* 对于门铃：开门取快件
* 对于哭声:照顾小孩



## 1.3 ARM系统中异常与中断处理流程

我们将母亲的处理过程抽象化：

* 母亲的头脑相当于CPU
  * 耳朵听到声音会发送信号给脑袋
  * 声音来源有很多种
    * 有远处的猫叫，门铃声，小孩哭声
  * 这些声音传入耳朵，再由耳朵传给大脑
  * 除了这些可以中断母亲的看书，还有其他情况，比如：
    * 身体不舒服
    * 有只蜘蛛掉下来
    * 对于特殊情况无法回避，必须立即处理



对于arm系统，异常与中断的硬件框图如下：

![](lesson\exception_irq\002_exception_on_arm.png)

所有的中断源(按键、定时器等)，它们发出的中断汇聚到**中断控制器**，
再由中断控制器发信号给CPU，告诉它发生了那些紧急情况。

除了这些中断，还有什么可以打断CPU的运行？

* 指令不对
* 数据访问有问题
* reset信号
* 等等，这些都可以打断断CPU，这些被称为**异常**
* 中断属于一种异常

ARM系统中如何处理异常与中断？重点在于**保存现场**以及**恢复现场**，
处理过程如下：

* 保存现场(各种寄存器)
* 处理异常(中断属于一种异常)
* 恢复现场 



细化一下，在ARM系统中如何使用异常(中断)？

* 初始化
  * 设置中断源，让它可以产生中断
  * 设置中断控制器(可以屏蔽某个中断，优先级)
  * 设置CPU总开关，使能中断

* 执行其他程序：正常程序
* 产生中断，举例：按下按键--->中断控制器--->CPU
* cpu每执行完一条指令都会检查有无中断/异常产生
* 发现有中断/异常产生，开始处理：
  * 保存现场
  * 分辨异常/中断，调用对于异常/中断的处理函数
  * 恢复现场



不同的芯片，不同的架构，在这方面的处理稍有差别：

* 保存/恢复现场：cortex M3/M4是硬件实现的，cortex A7是软件实现的
* CPU中止当前执行，跳转去执行处理异常的代码：也有差异
  * cortex M3/M4在向量表上放置的是函数地址
  * cortex A7在向量表上放置的是跳转指令